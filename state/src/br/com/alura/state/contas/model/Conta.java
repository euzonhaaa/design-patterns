package br.com.alura.state.contas.model;

import br.com.alura.state.contas.impl.Negativo;
import br.com.alura.state.contas.impl.Positivo;
import br.com.alura.state.contas.inter.EstadoDaConta;

public class Conta {

	private double saldo;
	private EstadoDaConta estadoAtual;

	public Conta(double saldo) {
		this.saldo = saldo;

		if (saldo > 0) {
			this.estadoAtual = new Positivo();
		} else {
			this.estadoAtual = new Negativo();
		}
	}

	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}

	public double getSaldo() {
		return saldo;
	}

	public void setEstadoAtual(EstadoDaConta estadoAtual) {
		this.estadoAtual = estadoAtual;
	}

	public EstadoDaConta getEstadoAtual() {
		return estadoAtual;
	}

	public void saque(double valor) {
		estadoAtual.saca(this, valor);
	}

	public void deposita(double valor) {
		estadoAtual.deposita(this, valor);
	}

}
