package br.com.alura.state.contas.impl;

import br.com.alura.state.contas.inter.EstadoDaConta;
import br.com.alura.state.contas.model.Conta;

public class Positivo implements EstadoDaConta {

	@Override
	public void saca(Conta conta, double valor) {
		if (valor > conta.getSaldo()) throw new RuntimeException("Saldo insuficiente!");

		double saldo = conta.getSaldo() - valor;

		conta.setSaldo(saldo);

		if (conta.getSaldo() < 0) {
			conta.setEstadoAtual(new Negativo());
		}
	}

	@Override
	public void deposita(Conta conta, double valor) {
		double valorPositivo = valor * 0.98;
		double saldo = conta.getSaldo() + valorPositivo;
		
		conta.setSaldo(saldo);
	}

}
