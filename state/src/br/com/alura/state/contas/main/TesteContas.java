package br.com.alura.state.contas.main;

import br.com.alura.state.contas.model.Conta;

public class TesteContas {

	public static void main(String[] args) {
		Conta conta = new Conta(-500);

		conta.deposita(100);
		
		System.out.println(conta.getSaldo());
		
	}

}
