package br.com.alura.state.impostos.impl;

import br.com.alura.state.impostos.inter.EstadoDeUmOrcamento;
import br.com.alura.state.impostos.model.Orcamento;

public class Aprovado implements EstadoDeUmOrcamento {
	
	private boolean descontoAplicado;

	@Override
	public void aplicaDescontoExtra(Orcamento orcamento) {
		if(!descontoAplicado) {
			double valor = orcamento.getValor() * 0.02;
			orcamento.setValor(orcamento.getValor() - valor);
			this.descontoAplicado = true;
		} else {
			throw new RuntimeException("Desconto já aplicado!");
		}
	}

	@Override
	public void aprova(Orcamento orcamento) {
		throw new RuntimeException("Orçamento já está aprovado!");
	}

	@Override
	public void reprova(Orcamento orcamento) {
		throw new RuntimeException("Orçamento aprovados não podem ser reprovados!");
	}

	@Override
	public void finaliza(Orcamento orcamento) {
		orcamento.setEstadoAtual(new Reprovado());
	}

}
