package br.com.alura.chain.requisicoes.inter;

import br.com.alura.chain.requisicoes.model.Conta;
import br.com.alura.chain.requisicoes.model.Requisicao;

public interface Resposta {

	void responde(Requisicao requisicao, Conta conta);

	void setProxima(Resposta resposta);

}
