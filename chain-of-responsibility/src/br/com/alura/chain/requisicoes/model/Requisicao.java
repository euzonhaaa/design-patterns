package br.com.alura.chain.requisicoes.model;

import br.com.alura.chain.requisicoes.formatos.Formato;

public class Requisicao {

	private Formato formato;

	public Requisicao(Formato formato) {
		this.formato = formato;
	}

	public Formato getFormato() {
		return formato;
	}

}
