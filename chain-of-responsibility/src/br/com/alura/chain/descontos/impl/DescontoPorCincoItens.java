package br.com.alura.chain.descontos.impl;

import br.com.alura.chain.descontos.inter.Desconto;
import br.com.alura.chain.descontos.model.Orcamento;

public class DescontoPorCincoItens implements Desconto {

	private Desconto proximo;

	@Override
	public double desconta(Orcamento orcamento) {
		// mais de 5 itens desconto
		if (orcamento.getItens().size() > 5) {
			return orcamento.getValor() * 0.1;
		} else {
			return proximo.desconta(orcamento);
		}

	}

	@Override
	public void setProximo(Desconto proximo) {
		this.proximo = proximo;

	}

}
