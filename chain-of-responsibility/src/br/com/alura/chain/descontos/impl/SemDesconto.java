package br.com.alura.chain.descontos.impl;

import br.com.alura.chain.descontos.inter.Desconto;
import br.com.alura.chain.descontos.model.Orcamento;

public class SemDesconto implements Desconto{

	@Override
	public double desconta(Orcamento orcamento) {
		return 0;
	}

	@Override
	public void setProximo(Desconto proximo) {
		
	}
	
	

}
