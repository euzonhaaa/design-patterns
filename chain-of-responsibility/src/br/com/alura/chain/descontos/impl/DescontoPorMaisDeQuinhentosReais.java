package br.com.alura.chain.descontos.impl;

import br.com.alura.chain.descontos.inter.Desconto;
import br.com.alura.chain.descontos.model.Orcamento;

public class DescontoPorMaisDeQuinhentosReais implements Desconto {

	private Desconto proximo;

	@Override
	public double desconta(Orcamento orcamento) {
		// segunda regra
		if (orcamento.getValor() > 500.0)
			return orcamento.getValor() * 0.07;
		else
			return proximo.desconta(orcamento);
	}

	@Override
	public void setProximo(Desconto proximo) {
		this.proximo = proximo;
	}
}
