package br.com.alura.decorator.filtro.main;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import br.com.alura.decorator.filtro.abs.Filtro;
import br.com.alura.decorator.filtro.impl.FiltroMaiorQue500MilReais;
import br.com.alura.decorator.filtro.impl.FiltroMenorQue100Reais;
import br.com.alura.decorator.filtro.impl.FiltroMesmoMes;
import br.com.alura.decorator.filtro.model.Conta;

public class TesteFiltrosContas {

	public static void main(String[] args) {
		
		Filtro contasFiltradas = new FiltroMaiorQue500MilReais(new FiltroMenorQue100Reais(new FiltroMesmoMes()));
		
		
		List<Conta> contas =  new ArrayList<>();
		contas.add(new Conta("Gabriel", 10, LocalDate.of(2020, 2, 20)));
		contas.add(new Conta("Kalliny", 14440009, LocalDate.of(2020, 1, 20)));
		
		contasFiltradas.filtra(contas);
		
	}

}
