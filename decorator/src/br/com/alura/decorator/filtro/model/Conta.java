package br.com.alura.decorator.filtro.model;

import java.time.LocalDate;

public class Conta {

	private String titular;

	private double valor;

	private LocalDate dataAbertura;

	public Conta(String titular, double valor, LocalDate dataAbertura) {
		this.titular = titular;
		this.valor = valor;
		this.dataAbertura = dataAbertura;
	}

	public String getTitular() {
		return titular;
	}

	public double getValor() {
		return valor;
	}

	public LocalDate getDataAbertura() {
		return dataAbertura;
	}

}
