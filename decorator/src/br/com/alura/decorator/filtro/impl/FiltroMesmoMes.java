package br.com.alura.decorator.filtro.impl;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import br.com.alura.decorator.filtro.abs.Filtro;
import br.com.alura.decorator.filtro.model.Conta;

public class FiltroMesmoMes extends Filtro {

	public FiltroMesmoMes(Filtro outroFiltro) {
		super(outroFiltro);
	}

	public FiltroMesmoMes() {

	}

	@Override
	public List<Conta> filtra(List<Conta> contas) {
		List<Conta> contasFiltradas = new ArrayList<Conta>();

		for (Conta conta : contas) {
			if (conta.getDataAbertura().getMonth() == LocalDate.now().getMonth()
					&& conta.getDataAbertura().getYear() == LocalDate.now().getYear()) {
				contasFiltradas.add(conta);
				System.out.println("Contas com data de abertura no mês corrente: " + conta.getTitular());
			}
		}

		contasFiltradas.addAll(proximo(contas));

		return contasFiltradas;
	}

}
