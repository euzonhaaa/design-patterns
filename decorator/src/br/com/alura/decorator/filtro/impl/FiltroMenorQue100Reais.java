package br.com.alura.decorator.filtro.impl;

import java.util.ArrayList;
import java.util.List;

import br.com.alura.decorator.filtro.abs.Filtro;
import br.com.alura.decorator.filtro.model.Conta;

public class FiltroMenorQue100Reais extends Filtro {

	public FiltroMenorQue100Reais(Filtro outroFiltro) {
		super(outroFiltro);
	}

	public FiltroMenorQue100Reais() {
	}

	@Override
	public List<Conta> filtra(List<Conta> contas) {
		List<Conta> filtrada = new ArrayList<Conta>();

		for (Conta c : contas) {
			if (c.getValor() < 100) {
				filtrada.add(c);
				System.out.println("Contas com saldo menor que R$ 100,00: " + c.getTitular());
			}
		}

		filtrada.addAll(proximo(contas));

		return filtrada;
	}
}
