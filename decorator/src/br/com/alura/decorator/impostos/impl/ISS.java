package br.com.alura.decorator.impostos.impl;

import br.com.alura.decorator.impostos.inter.Imposto;
import br.com.alura.decorator.impostos.model.Orcamento;

public class ISS extends Imposto {

	public ISS(Imposto outroImposto) {
		super(outroImposto);
	}

	public ISS() {
	}

	@Override
	public double calcula(Orcamento orcamento) {
		return orcamento.getValor() * 0.06 + calculoDoOutroImposto(orcamento);
	}
	
}
