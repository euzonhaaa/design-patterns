package br.com.alura.decorator.impostos.impl;

import br.com.alura.decorator.impostos.inter.Imposto;
import br.com.alura.decorator.impostos.model.Orcamento;

public class ICCC extends Imposto {

	@Override
	public double calcula(Orcamento orcamento) {
		return orcamento.getValor() * 0.06;
	}


}
