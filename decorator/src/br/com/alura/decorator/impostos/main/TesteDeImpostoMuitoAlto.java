package br.com.alura.decorator.impostos.main;

import br.com.alura.decorator.impostos.impl.ICMS;
import br.com.alura.decorator.impostos.impl.ImpostoMuitoAlto;
import br.com.alura.decorator.impostos.model.Orcamento;

public class TesteDeImpostoMuitoAlto {

	public static void main(String[] args) {
		ImpostoMuitoAlto impostoMuitoAlto = new ImpostoMuitoAlto(new ICMS());
		
		Orcamento orcamento = new Orcamento(500);
		
		System.out.println(impostoMuitoAlto.calcula(orcamento));
		
	}

}
