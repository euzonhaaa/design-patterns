package br.com.alura.builder;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import br.com.alura.builder.model.ItemDaNota;
import br.com.alura.builder.model.NotaFiscal;

public class NotaFiscalBuilder {

	private String razaoSocial;
	private String cnpj;
	private List<ItemDaNota> todosItens = new ArrayList<>();
	private double valorBruto;
	private double impostos;
	private LocalDate dataDeEmissao;
	private String observacoes;

	public NotaFiscalBuilder paraEmpresa(String razaoSocial) {
		this.razaoSocial = razaoSocial;
		return this;
	}

	public NotaFiscalBuilder comCnpj(String cnpj) {
		this.cnpj = cnpj;
		return this;
	}

	public NotaFiscalBuilder com(ItemDaNota item) {
		this.todosItens.add(item);

		this.valorBruto += item.getValor();
		this.impostos += item.getValor() * 0.05;

		return this;
	}

	public NotaFiscalBuilder naData(LocalDate data) {
		this.dataDeEmissao = data;
		return this;
	}

	public NotaFiscalBuilder comObservacoes(String observacoes) {
		this.observacoes = observacoes;
		return this;
	}

	public NotaFiscal constroi() {
		return new NotaFiscal(razaoSocial, cnpj, dataDeEmissao, valorBruto, impostos, todosItens, observacoes);
	}

}
