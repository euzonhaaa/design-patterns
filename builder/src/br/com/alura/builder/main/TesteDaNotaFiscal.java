package br.com.alura.builder.main;

import br.com.alura.builder.NotaFiscalBuilder;
import br.com.alura.builder.model.ItemDaNota;
import br.com.alura.builder.model.NotaFiscal;

public class TesteDaNotaFiscal {

	public static void main(String[] args) {
		NotaFiscalBuilder notaFiscalBuilder = new NotaFiscalBuilder();
		
		notaFiscalBuilder
			.paraEmpresa("Caelum Ensino e Inovação")
			.comCnpj("12.345.678/0001-12")
			.comItem(new ItemDaNota("item 1", 200.0))
			.comItem(new ItemDaNota("item 2", 300.0))
			.comItem(new ItemDaNota("item 3", 400.0));
		
		NotaFiscal notaFiscal = notaFiscalBuilder.constroi();
		
		System.out.println(notaFiscal.getCnpj());
		
	}

}
