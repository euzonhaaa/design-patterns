package br.com.alura.observer;

import br.com.alura.model.NotaFiscal;

public class NotaFiscalDao implements AcaoAposGerarNota {

	@Override
	public void executa(NotaFiscal nf) {
		System.out.println("Salvei no banco");
	}

}
