package br.com.alura.strategy.impostos.negocio;

import br.com.alura.strategy.impostos.inter.Imposto;
import br.com.alura.strategy.impostos.model.Orcamento;

public class CalculadorDeImpostos {

	public void realizaCalculo(Orcamento orcamento, Imposto impostoQualquer) {
		double icms = impostoQualquer.calcula(orcamento);

		System.out.println(icms);
	}

}
