package br.com.alura.strategy.investimentos.impl;

import br.com.alura.strategy.investimentos.inter.Investimento;
import br.com.alura.strategy.investimentos.model.Conta;

public class Conservador implements Investimento {

	@Override
	public double calculaInvestimento(Conta conta) {
		return conta.getSaldo() * 0.008;
	}

}
