package br.com.alura.strategy.impostos.main;

import br.com.alura.strategy.impostos.impl.ICCC;
import br.com.alura.strategy.impostos.model.Orcamento;

public class TesteICCC {
	
	public static void main(String[] args) {
		ICCC iccc = new ICCC();
		Orcamento orcamento = new Orcamento(5000);
		
		System.out.println(iccc.calcula(orcamento));
		
	}
}
