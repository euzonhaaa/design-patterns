package br.com.alura.strategy.impostos.main;

import br.com.alura.strategy.impostos.impl.ICMS;
import br.com.alura.strategy.impostos.impl.ISS;
import br.com.alura.strategy.impostos.model.Orcamento;
import br.com.alura.strategy.impostos.negocio.CalculadorDeImpostos;

public class TesteDeImpostos {

	public static void main(String[] args) {
		ISS iss = new ISS();
		ICMS icms = new ICMS();
		
		Orcamento orcamento = new Orcamento(500.0);
		CalculadorDeImpostos calculadorDeImpostos = new CalculadorDeImpostos();
		
		calculadorDeImpostos.realizaCalculo(orcamento, iss);
		calculadorDeImpostos.realizaCalculo(orcamento, icms);
	}

}
