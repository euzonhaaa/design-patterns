package br.com.alura.strategy.impostos.impl;

import br.com.alura.strategy.impostos.inter.Imposto;
import br.com.alura.strategy.impostos.model.Orcamento;

public class ISS implements Imposto {

	@Override
	public double calcula(Orcamento orcamento) {
		return orcamento.getValor() * 0.06;
	}

}
